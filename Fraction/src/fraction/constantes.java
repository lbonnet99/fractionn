package fraction;

public enum constantes
{
	
	ZERO(0,1),
	UN(1,1);	
	
	private int num;
	private int denom;
	
	private constantes(int num, int denom)
	{
		this.num = num;
		this.denom = denom;
	}
	
	public int getnum()
	{
		return num;
	}
	
	public int getdenom()
	{
		return denom;
	}
	
};
